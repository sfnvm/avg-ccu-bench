package io.sfnvm.ccu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CcuApplication {

  public static void main(String[] args) {
    SpringApplication.run(CcuApplication.class, args);
  }

}
