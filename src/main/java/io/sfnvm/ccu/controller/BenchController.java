package io.sfnvm.ccu.controller;

import io.sfnvm.ccu.service.BenchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("basic-setting")
public class BenchController {
  @Autowired
  private BenchService benchService;

  @GetMapping
  public ResponseEntity<?> bench() throws Exception {
    return ResponseEntity.ok(benchService.findById(1));
  }
}
