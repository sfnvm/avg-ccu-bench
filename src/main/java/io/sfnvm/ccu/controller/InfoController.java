package io.sfnvm.ccu.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("app-info")
public class InfoController {
  @GetMapping("version")
  public ResponseEntity<?> ping() {
    return ResponseEntity.ok("pong");
  }
}
