package io.sfnvm.ccu.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "ads_popup")
@Data
@Builder
@RequiredArgsConstructor
@AllArgsConstructor
public class AdsPopup {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  private String adLocation;
  private Integer frequency;
  private Integer duration;

  private String time; // ss:mm:hh
  private String endTime; // ss:mm:hh

  private String adTarget;

  @ElementCollection
  private List<String> adImage;

  @ElementCollection
  private List<String> adImageMobile;

  @ElementCollection
  private List<String> adImageTv;

  private String adLinkClick;
}
