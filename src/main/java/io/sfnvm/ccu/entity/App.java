package io.sfnvm.ccu.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "app")
@Data
@Builder
@RequiredArgsConstructor
@AllArgsConstructor
public class App {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  private String navTopBg;
  private String mainBg;
  private String navBottomBg;
  private String animationLogo;
  private String logo;
}
