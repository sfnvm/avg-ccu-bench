package io.sfnvm.ccu.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "bench_model")
@Data
@Builder
@RequiredArgsConstructor
@AllArgsConstructor
public class BenchEntity {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  private String drmQnetToday;
  private String linkShareMovie;
  private String drmQnet;
  private String pingTime; // this shit should be mesure manually

  // TODO
  @OneToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "appId")
  private App app;

  @OneToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "adsPopupId")
  private AdsPopup adsPopup;

  private Double newestVersion;
  private String newestVersion1;
  private boolean isForceUpdate;
  private boolean isUpdate;
  private String smartcardConnectGuideline;
  private Integer flagEvent;
  private Integer maxRandomCountdownEvent;
}
