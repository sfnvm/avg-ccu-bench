package io.sfnvm.ccu.io.res;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@Builder
@RequiredArgsConstructor
@AllArgsConstructor
public class BenchModel {
  private int id;

  private String drmQnetToday;
  private String linkShareMovie;
  private String drmQnet;
  private String pingTime; // this shit should be mesure manually
}
