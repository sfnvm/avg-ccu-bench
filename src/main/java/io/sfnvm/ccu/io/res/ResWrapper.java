package io.sfnvm.ccu.io.res;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ResWrapper<T> {
  @JsonProperty("status_code")
  private Integer statusCode;

  @JsonProperty("error_code")
  private Integer errorCode;

  @JsonProperty("timestamp")
  private Long timestamp;

  @JsonProperty("error_description")
  private Long errorDescription;

  @JsonProperty("response_object")
  private T responseObject;
}
