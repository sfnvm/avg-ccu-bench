package io.sfnvm.ccu.repository;

import io.sfnvm.ccu.entity.BenchEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BenchRepository extends JpaRepository<BenchEntity, Integer> {
}
