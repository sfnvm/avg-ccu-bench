package io.sfnvm.ccu.service;

import io.sfnvm.ccu.entity.BenchEntity;
import io.sfnvm.ccu.repository.BenchRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BenchService {
  @Autowired
  private BenchRepository benchRepository;

  public BenchEntity findById(int id) throws Exception {
    return benchRepository.findById(id).orElseThrow(() -> new Exception("Bench id not found"));
  }
}
